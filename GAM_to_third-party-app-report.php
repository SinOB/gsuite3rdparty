<?php

// get https://developers.google.com/drive/api/v2/downloads installed
require_once 'vendor/autoload.php';
header('Content-Type: text/html; charset=UTF-8');

//
// NB: make sure that initial csv from Google Drive is manually converted to UTF8 before running script, otherwise special characters in the display names will be borked (bloody Google Drive)
// Script to parse through GAM report (extracted to CSV) and determine if identified apps are low or high risk
//
// Based on https://github.com/slackhq/gsuite-oauth-third-party-app-report/wiki
// specifically https://github.com/slackhq/gsuite-oauth-third-party-app-report/blob/master/third-party-app-report.gs
// step 2
// Would prefer to have used the original script above, unfortunately keeps timing out on our dataset


$HIGH_RISK_ACCESS = array(
    "https://mail.google.com",
    "https://www.googleapis.com/auth/gmail.compose",
    "https://www.googleapis.com/auth/gmail.insert",
    "https://www.googleapis.com/auth/gmail.labels",
    "https://www.googleapis.com/auth/gmail.modify",
    "https://www.googleapis.com/auth/gmail.readonly",
    "https://www.googleapis.com/auth/gmail.send",
    "https://www.googleapis.com/auth/gmail.settings.basic",
    "https://www.googleapis.com/auth/gmail.settings.sharing",
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.file",
    "https://www.googleapis.com/auth/drive.metadata",
    "https://www.googleapis.com/auth/drive.photos.readonly",
    "https://www.googleapis.com/auth/drive.readonly",
    "https://www.googleapis.com/auth/drive.scripts",
    "https://www.googleapis.com/auth/ediscovery",
    "https://www.googleapis.com/auth/ediscovery.readonly",
    "https://www.googleapis.com/auth/admin.directory.customer",
    "https://www.googleapis.com/auth/admin.directory.customer.readonly",
    "https://www.googleapis.com/auth/admin.directory.device.chromeos",
    "https://www.googleapis.com/auth/admin.directory.device.chromeos.readonly",
    "https://www.googleapis.com/auth/admin.directory.device.mobile",
    "https://www.googleapis.com/auth/admin.directory.device.mobile.action",
    "https://www.googleapis.com/auth/admin.directory.device.mobile.readonly",
    "https://www.googleapis.com/auth/admin.directory.domain",
    "https://www.googleapis.com/auth/admin.directory.domain.readonly",
    "https://www.googleapis.com/auth/admin.directory.group",
    "https://www.googleapis.com/auth/admin.directory.group.member",
    "https://www.googleapis.com/auth/admin.directory.group.member.readonly",
    "https://www.googleapis.com/auth/admin.directory.group.readonly",
    "https://www.googleapis.com/auth/admin.directory.notifications",
    "https://www.googleapis.com/auth/admin.directory.orgunit",
    "https://www.googleapis.com/auth/admin.directory.orgunit.readonly",
    "https://www.googleapis.com/auth/admin.directory.resource.calendar",
    "https://www.googleapis.com/auth/admin.directory.resource.calendar.readonly",
    "https://www.googleapis.com/auth/admin.directory.rolemanagement",
    "https://www.googleapis.com/auth/admin.directory.rolemanagement.readonly",
    "https://www.googleapis.com/auth/admin.directory.user",
    "https://www.googleapis.com/auth/admin.directory.user.alias",
    "https://www.googleapis.com/auth/admin.directory.user.alias.readonly",
    "https://www.googleapis.com/auth/admin.directory.user.readonly",
    "https://www.googleapis.com/auth/admin.directory.user.security",
    "https://www.googleapis.com/auth/admin.directory.userschema",
    "https://www.googleapis.com/auth/admin.directory.userschema.readonly",
    "https://www.googleapis.com/auth/admin.reports.audit.readonly",
    "https://www.googleapis.com/auth/admin.reports.usage.readonly"
);

/*
// Determine if the app uses one of the permissions defined as high
// risk then flag it as a high risk app
*/
function getRisk($scope){
	global $HIGH_RISK_ACCESS;
	$array = explode(' ', $scope);
	if(array_intersect($array, $HIGH_RISK_ACCESS))
	{
		return "TRUE";
	}
	return "FALSE";
}

/*
//Get counts of token usage
*/
function step2() {
    $countsRows = array();
    $countsRows[] = array(
        "numInstalls",
        "displayText",
        "clientId",
        "highRisk",
        "scopes"
    );

	setlocale(LC_CTYPE, "en.UTF8");
    $fileName = "../../OAuthTokens.csv";
    $count=0;

    $destinationFile = "Output_3rd_party_app_report.csv";
	$csvDelimiter="*";

    $apps = array();


	if (($fp = fopen($fileName, "r")) !== FALSE) {
		while ($row = fgetcsv($fp)) {
			$count++;

			// skip saving the first row - its just a header
			if($count==1){
				continue;
			}

			//if($row[1] =="1095133494869.apps.googleusercontent.com"){
			//	print_r ($row);
		//	}

			// have we come across this app before?
			if(isset($apps[$row[1]])){
				//incremment the count of installs by 1
				$installedCount = $apps[$row[1]]['numInstalls']+1;
			} else {
				//add the app to our array of apps and set the number of installs to 1
				$apps[$row[1]]=array();
				$installedCount=1;
			}

			$apps[$row[1]]['numInstalls']=$installedCount;
			$apps[$row[1]]['displayText']= $row[2];
			$apps[$row[1]]['Match']= getRisk($row[6]);
			$apps[$row[1]]['Scope']= $row[6];

			//debug
			//print_r ($apps[$row[1]]);
		}

		fclose($fp);
	}

	// next up, output all the results to a csv ...


	$fp = fopen($destinationFile, 'w');
	/* let''s set the encoding properly for those special characters (i.e. UTF-8 that MOFO)*/
	fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
	$headers = array("numInstalls","displayText","clientId","highRisk","scopes");
	fputcsv($fp, $headers, $csvDelimiter);

	foreach ($apps as $key => $value) {
		$appRow = array($value['numInstalls'],$key, $value['displayText'],$value['Match'],$value['Scope']);
	    fputcsv($fp, $appRow, $csvDelimiter);
	}

fclose($fp);

}

step2();


?>