# README #

Command line script to evaluate GSuite OAuth connected apps that can be considered to have High Risk Access. 

Heavily based on google https://github.com/slackhq/gsuite-oauth-third-party-app-report/blob/master/third-party-app-report.gs. 

Usable in the case where GAM returns too many records for the third-party-app-report.gs to be able to process via the Google Sheets App script editor. 

__Input:__ A csv file of the results of GAM command `gam all users print tokens todrive`

__Output:__ A csv file with the following for each App <Count of installs, App Name, App Client ID, Hih Risk (Yes/No), Scope>


### What is this repository for? ###


* Taking a GAM generated report of all apps for all users and creating a new report, listing all the unique apps and identifying if they are high or 
low risk based on their listed permissions.
* Version 0.2

### How do I get set up? ###

* Standard commandline PHP script. Runs on LAMP/WAMP

##Step 1##
* Install GAM by jay0lee: https://github.com/jay0lee/GAM
* Run the command:
`gam all users print tokens todrive`

##Step 2##
* Navigate to Google Drive (drive.google.com) and open the new sheet generated by GAM
* Go to File > download as > Comma-separated values (.csv, current sheeet)

##Step 3##
* Download the script in this repo "GAM_to_third-party-app-report.php"
* Update the php script with the location of your CSV
* run the php script from your command line
* Voilà, a csv has been created listing your apps and identifying which are high risk and which are low. 


### Acknowledgments ###

* https://github.com/jay0lee - for building GAM - an awesome tool for interacting with G Suite.
* https://github.com/slackhq - for writing and sharing the most useful Gsuite script gsuite-oauth-third-party-app-report (upon which code this scipt is based)